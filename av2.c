#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef _WIN32
	#define READ(x) do{ system ("/bin/stty raw");\
		x = getchar ();\
		system ("/bin/stty cooked");}while(0)
#else 
	#include <windows.h>
	#define READ(x) x = getch()
#endif

//+2 representa as bordas, e +1 da coluna do \0.
#define LIN 30+2
#define COL 100+2+1

#define METQ 15

//Constantes para intro
#define LINI 48
#define COLI 50

//Posicoes serão dadas por y para linha e x para coluna.

//Struct que vai conter a posição da nave no mapa do jogo. 
typedef struct position_s{

	int yL;

	int xC;

}position_t;

//Struct de posicao do meteoro no mapa do jogo.
typedef struct meteoro_s{

	int yML;
	int xMC;

}meteoro_t;

void mov_meteoro(char mapSpace[LIN][COL], meteoro_t p[METQ], int *contL, int *contC, int *quantMeteoro);

void mapCreate(char mapSpace[LIN][COL], int *contC, int *contL);

void intro(void);

int main(){

		position_t position;

		meteoro_t meteoro[METQ];


		int contC, contL, aux, hit;

		int temp = 0, quantMeteoro = 0, points = 0;

		char comand;

		char mapSpace[LIN][COL];

		position.yL = 14;

		position.xC = 15;

		mapCreate(mapSpace, &contC, &contL);

		intro();

		sleep(1);

		printf(" ______  ______   ____   __  __  ____   _  _\n");
		printf("|  __  ||   _  | | ___|  \\ \\/ / | __ | | || |\n");
		printf("| |__| | | |_| | | |_     \\  /  ||  || | || |\n");
		printf("|  __  | |  ___| |  _|     ||   ||  || | || | \n");
		printf("| |  | | | |\\ \\  | |__     ||   ||__|| | || |\n");
		printf("|_|  |_| |_| \\_\\ |____|    ||   |____| |____|\n");
		printf(" ______   ____   ______   ____  __  __  \n");
		printf("|   _  | | ___| |  __  | |  _ \\ \\ \\/ / \n");
		printf(" | |_| | | |_   | |__| | | | | | \\  / \n");
		printf(" |  ___| |  _|  |  __  | | | | |  ||\n");
		printf(" | |\\ \\  | |__  | |  | | | |_| |  ||\n");
		printf(" |_| \\_\\ |____| |_|  |_| |____/   ||\n");

		sleep(1);

		srand(time(NULL));

		for(aux = 0; aux <METQ; aux++){

			meteoro[aux].yML = 2 + (rand() % 25);

		}

		for(aux = 0; aux <METQ; aux++){

			meteoro[aux].xMC = 90;

		}


		do{

			//Mudar a opção de limpeza de tela de acordo com o sistema que estiver usando.
			//system("cls");
			system("clear");

			if(position.yL == 0 || position.yL + 2 == LIN-1){

				//Mudar a opção de limpeza de tela de acordo com o sistema que estiver usando.
				//system("cls");
				system("clear");

				printf("______               __    __\n");
				printf("|  ____|   ______    |  \\  /  |  _____\n");
				printf("| |  ___  |____  |   |   \\/ / | |  ___|\n" );
				printf("| | |   |  ____| |   |  \\__/| | | |__\n" );
				printf("| |__| |  |  _   |   |  |   | | |  __|\n");
				printf("|______|  | |_|  |_  |__|   |_| | |___\n");
				printf("          |________|            |_____|\n");
				printf("     _____  __      __   _____   _______\n");
				printf("    |  _  | \\ \\    / /  |  ___| |_   _  |\n");
				printf("    | | | |  \\ \\  / /   | |__     | |_| |\n");
				printf("    | |_| |   \\ \\/ /    |  __|    |  ___|\n");
				printf("    |_____|    \\__/     | |___    |  \\ \\\n");
				printf("                        |_____|   |__|\\_\\\n");

				printf("\n\n\n 				GAME POINTS: %d\n\n\n", points);

				break;

			}



			if(temp % 9 == 0 && quantMeteoro < METQ){

				quantMeteoro++;

				temp = 0;	

			}

			

			

			//identifica quando um meteoro chega ao final, soma ponto e da inicio a um novo meteoro
			if(meteoro[0].xMC == 1){

				mapSpace[meteoro[0].yML][meteoro[0].xMC] = ' ';
				mapSpace[meteoro[0].yML][meteoro[0].xMC+1] = ' ';
				mapSpace[meteoro[0].yML][meteoro[0].xMC+2] = ' ';
				mapSpace[meteoro[0].yML][meteoro[0].xMC+3] = ' ';
				mapSpace[meteoro[0].yML][meteoro[0].xMC+4] = ' ';
				mapSpace[meteoro[0].yML][meteoro[0].xMC+5] = ' ';
				mapSpace[meteoro[0].yML][meteoro[0].xMC+6] = ' ';
				mapSpace[meteoro[0].yML][meteoro[0].xMC+7] = ' ';
				mapSpace[meteoro[0].yML][meteoro[0].xMC+8] = ' ';

				mapSpace[meteoro[0].yML+1][meteoro[0].xMC] = ' ';
				mapSpace[meteoro[0].yML+1][meteoro[0].xMC+1] = ' ';
				mapSpace[meteoro[0].yML+1][meteoro[0].xMC+2] = ' ';
				mapSpace[meteoro[0].yML+1][meteoro[0].xMC+3] = ' ';
				mapSpace[meteoro[0].yML+1][meteoro[0].xMC+4] = ' ';
				mapSpace[meteoro[0].yML+1][meteoro[0].xMC+5] = ' ';
				mapSpace[meteoro[0].yML+1][meteoro[0].xMC+6] = ' ';
				mapSpace[meteoro[0].yML+1][meteoro[0].xMC+7] = ' ';
				mapSpace[meteoro[0].yML+1][meteoro[0].xMC+8] = ' ';

				mapSpace[meteoro[0].yML+2][meteoro[0].xMC] = ' ';
				mapSpace[meteoro[0].yML+2][meteoro[0].xMC+1] = ' ';
				mapSpace[meteoro[0].yML+2][meteoro[0].xMC+2] = ' ';
				mapSpace[meteoro[0].yML+2][meteoro[0].xMC+3] = ' ';
				mapSpace[meteoro[0].yML+2][meteoro[0].xMC+4] = ' ';
				mapSpace[meteoro[0].yML+2][meteoro[0].xMC+5] = ' ';
				mapSpace[meteoro[0].yML+2][meteoro[0].xMC+6] = ' ';
				mapSpace[meteoro[0].yML+2][meteoro[0].xMC+7] = ' ';
				mapSpace[meteoro[0].yML+2][meteoro[0].xMC+8] = ' ';

				points++;

				for(aux = 0; aux < quantMeteoro; aux++){

					meteoro[aux].yML = meteoro[aux+1].yML;

					meteoro[aux].xMC = meteoro[aux+1].xMC;							

				}

				meteoro[quantMeteoro -1].yML = 2 + (rand() % 25);
				meteoro[quantMeteoro -1].xMC = 90;

			}

			

			//apaga o meteoro no mapa para movimenta-lo a proxima coordenada	
			if(comand != 27){
						
				for(aux = 0; aux < quantMeteoro; aux++){

					
					mapSpace[meteoro[aux].yML][meteoro[aux].xMC] = ' ';
					mapSpace[meteoro[aux].yML][meteoro[aux].xMC+1] = ' ';
					mapSpace[meteoro[aux].yML][meteoro[aux].xMC+2] = ' ';
					mapSpace[meteoro[aux].yML][meteoro[aux].xMC+3] = ' ';
					mapSpace[meteoro[aux].yML][meteoro[aux].xMC+4] = ' ';
					mapSpace[meteoro[aux].yML][meteoro[aux].xMC+5] = ' ';
					mapSpace[meteoro[aux].yML][meteoro[aux].xMC+6] = ' ';
					mapSpace[meteoro[aux].yML][meteoro[aux].xMC+7] = ' ';
					mapSpace[meteoro[aux].yML][meteoro[aux].xMC+8] = ' ';

					mapSpace[meteoro[aux].yML+1][meteoro[aux].xMC] = ' ';
					mapSpace[meteoro[aux].yML+1][meteoro[aux].xMC+1] = ' ';
					mapSpace[meteoro[aux].yML+1][meteoro[aux].xMC+2] = ' ';
					mapSpace[meteoro[aux].yML+1][meteoro[aux].xMC+3] = ' ';
					mapSpace[meteoro[aux].yML+1][meteoro[aux].xMC+4] = ' ';
					mapSpace[meteoro[aux].yML+1][meteoro[aux].xMC+5] = ' ';
					mapSpace[meteoro[aux].yML+1][meteoro[aux].xMC+6] = ' ';
					mapSpace[meteoro[aux].yML+1][meteoro[aux].xMC+7] = ' ';
					mapSpace[meteoro[aux].yML+1][meteoro[aux].xMC+8] = ' ';

					mapSpace[meteoro[aux].yML+2][meteoro[aux].xMC] = ' ';
					mapSpace[meteoro[aux].yML+2][meteoro[aux].xMC+1] = ' ';
					mapSpace[meteoro[aux].yML+2][meteoro[aux].xMC+2] = ' ';
					mapSpace[meteoro[aux].yML+2][meteoro[aux].xMC+3] = ' ';
					mapSpace[meteoro[aux].yML+2][meteoro[aux].xMC+4] = ' ';
					mapSpace[meteoro[aux].yML+2][meteoro[aux].xMC+5] = ' ';
					mapSpace[meteoro[aux].yML+2][meteoro[aux].xMC+6] = ' ';
					mapSpace[meteoro[aux].yML+2][meteoro[aux].xMC+7] = ' ';
					mapSpace[meteoro[aux].yML+2][meteoro[aux].xMC+8] = ' ';

					meteoro[aux].xMC--;

					temp++;

				}

						

			}

			for(contL = 0; contL < LIN; contL++){
				for(contC = 0; contC < COL-1; contC++){


					//Desenhar a nave no mapa
					if(contL == position.yL && contC == position.xC){

						mapSpace[position.yL][position.xC] = ' ';
						if(mapSpace[position.yL][position.xC+3] != ' ') hit = 1;
						mapSpace[position.yL][position.xC+3] = '\\';
						if(mapSpace[position.yL][position.xC+5] != ' ') hit = 1;
						mapSpace[position.yL][position.xC+5] = '\\';
						if(mapSpace[position.yL][position.xC+6] != ' ') hit = 1;
						mapSpace[position.yL][position.xC+6] = '_';
						if(mapSpace[position.yL][position.xC+7] != ' ') hit = 1;
						mapSpace[position.yL][position.xC+7] = '_';
						if(mapSpace[position.yL][position.xC+8] != ' ') hit = 1;
						mapSpace[position.yL][position.xC+8] = '_';
						if(mapSpace[position.yL][position.xC+9] != ' ') hit = 1;
						mapSpace[position.yL][position.xC+9] = '_';
						
						mapSpace[position.yL+1][position.xC] = '#';
						
						mapSpace[position.yL+1][position.xC+1] = '#';
						
						mapSpace[position.yL+1][position.xC+2] = '#';
						if(mapSpace[position.yL+1][position.xC+3] != ' ') hit = 1;
						mapSpace[position.yL+1][position.xC+3] = '[';
						if(mapSpace[position.yL+1][position.xC+4] != ' ') hit = 1;
						mapSpace[position.yL+1][position.xC+4] = '=';
						if(mapSpace[position.yL+1][position.xC+5] != ' ') hit = 1;
						mapSpace[position.yL+1][position.xC+5] = '=';
						if(mapSpace[position.yL+1][position.xC+6] != ' ') hit = 1;
						mapSpace[position.yL+1][position.xC+6] = '_';
						if(mapSpace[position.yL+1][position.xC+7] != ' ') hit = 1;
						mapSpace[position.yL+1][position.xC+7] = '_';
						if(mapSpace[position.yL+1][position.xC+8] != ' ') hit = 1;
						mapSpace[position.yL+1][position.xC+8] = '_';
						if(mapSpace[position.yL+1][position.xC+9] != ' ') hit = 1;
						mapSpace[position.yL+1][position.xC+9] = '_';
						if(mapSpace[position.yL+1][position.xC+10] != ' ') hit = 1;
						mapSpace[position.yL+1][position.xC+10] = '>';
						if(mapSpace[position.yL+2][position.xC+3] != ' ') hit = 1;
						mapSpace[position.yL+2][position.xC+3] = '/';
						if(mapSpace[position.yL+2][position.xC+5] != ' ') hit = 1;
						mapSpace[position.yL+2][position.xC+5] = '/';

					}

					
					//system("pause");
					mov_meteoro(mapSpace, meteoro, &contL, &contC, &quantMeteoro);
					//identifica se teve colisao e termina o programa, com bugs
					if(hit != 0){

						//Mudar a opção de limpeza de tela de acordo com o sistema que estiver usando.
						//system("cls");
						system("clear");

						printf("______               __    __\n");
						printf("|  ____|   ______    |  \\  /  |  _____\n");
						printf("| |  ___  |____  |   |   \\/ / | |  ___|\n" );
						printf("| | |   |  ____| |   |  \\__/| | | |__\n" );
						printf("| |__| |  |  _   |   |  |   | | |  __|\n");
						printf("|______|  | |_|  |_  |__|   |_| | |___\n");
						printf("          |________|            |_____|\n");
						printf("     _____  __      __   _____   _______\n");
						printf("    |  _  | \\ \\    / /  |  ___| |_   _  |\n");
						printf("    | | | |  \\ \\  / /   | |__     | |_| |\n");
						printf("    | |_| |   \\ \\/ /    |  __|    |  ___|\n");
						printf("    |_____|    \\__/     | |___    |  \\ \\\n");
						printf("                        |_____|   |__|\\_\\\n");

						printf("\n\n\n 				GAME POINTS: %d\n\n\n", points);

						sleep(5);

						exit(0);

					}
		 			
					printf("%c", mapSpace[contL][contC]);


				}

				if(contL == 1){

					printf("	PONTUAÇÃO: %d", points);

				}else if(contL == 2){

					printf("	By: -BONDE ");

				}

				printf("\n");

			}

			READ(comand);


			//Identificar movimentação de jogador
			if(comand != 27){

						//Apaga desenho da nave na matriz
						mapSpace[position.yL][position.xC] = ' ';
						mapSpace[position.yL][position.xC+3] = ' ';
						mapSpace[position.yL][position.xC+5] = ' ';
						mapSpace[position.yL][position.xC+6] = ' ';
						mapSpace[position.yL][position.xC+7] = ' ';
						mapSpace[position.yL][position.xC+8] = ' ';
						mapSpace[position.yL][position.xC+9] = ' ';
						mapSpace[position.yL+1][position.xC] = ' ';
						mapSpace[position.yL+1][position.xC+1] = ' ';
						mapSpace[position.yL+1][position.xC+2] = ' ';
						mapSpace[position.yL+1][position.xC+3] = ' ';
						mapSpace[position.yL+1][position.xC+4] = ' ';
						mapSpace[position.yL+1][position.xC+5] = ' ';
						mapSpace[position.yL+1][position.xC+6] = ' ';
						mapSpace[position.yL+1][position.xC+7] = ' ';
						mapSpace[position.yL+1][position.xC+8] = ' ';
						mapSpace[position.yL+1][position.xC+9] = ' ';
						mapSpace[position.yL+1][position.xC+10] = ' ';
						mapSpace[position.yL+2][position.xC+3] = ' ';
						mapSpace[position.yL+2][position.xC+5] = ' ';

				switch(comand){

					case 119:

						position.yL--;

					break;

					case 115:

						position.yL++;

					break;

				}

			}


		}while(comand != 27);


}		
	



//Função para criar/resetar o mapa do jogo

void mapCreate(char mapSpace[LIN][COL], int *contC, int *contL){

	for(*contL = 0; *contL < LIN; (*contL)++){
		for(*contC = 0; *contC < COL; (*contC)++){

			if(*contC == COL-1){

				mapSpace[*contL][*contC] = '\0';

			}else if(*contC == 0){

				mapSpace[*contL][*contC] = '|';

			}else if(*contC == COL - 2){

				mapSpace[*contL][*contC] = '|';

			}

			if(*contL == 0 && *contC != COL-1){

				mapSpace[*contL][*contC] = '-';

			}else if(*contL == LIN-1 && *contC != COL-1){

				mapSpace[*contL][*contC] = '-';

			}

			if(*contC != 0 && *contC != COL-1 && *contC != COL - 2 && *contL != 0 && *contL != LIN-1){

				mapSpace[*contL][*contC] = ' ';

			}


		}

	}

}

void intro(void){
	int i, j, k;
	char *matr[LINI];
	char *temp;

	static char foguete[LINI][COLI] =
	{
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', '/', '\\', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '|', '=', '=', '|', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '|', ' ', ' ', '|', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '|', ' ', ' ', '|', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '/', ' ', ' ', '\\', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', '|', ' ', ' ', ' ', ' ', '|', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', '|', ' ', ' ', ' ', ' ', '|', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', '|', ' ', '|', ' ', ' ', '|', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '/', '|', ' ', '|', ' ', ' ', '|', '\\', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', '/', ' ', '|', ' ', '|', ' ', ' ', '|', ' ', '\\', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '/', '_', '_', '|', '_', '_', '_', '_', '|', '_', '_', '\\', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', '/', '_', '\\', '/', '_', '\\', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', '#', '#', '#', '#', '#', '#', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', '#', '#', '#', '#', '#', '#', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '#', '#', '#', '#', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', '#', '#', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', '#', '#', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', '#', '#', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '},
		{'*', ' ', '*', ' ', '*', ' ', '*', '#', '#', ' ', '*', ' ', '*', ' ', '*', ' ', '*', ' '}
	};

	for(i = 0; i < LINI; i++){
		*(matr+i) = *(foguete+i);
	}

	for(i = 0; i < LINI; i++){
		for(j = 0; j < LINI-20; j++){
			for(k = 0; k < COLI; k++){
				printf("%c",*(*(matr+j)+k));
			}
			printf("\n");
		}
		usleep(250000);
		temp = *matr;
		for(j = 0; j < LINI-1; j++){
			*(matr+j) = *(matr+j+1);
		}
		*(matr+LINI-1) = temp;

		//Mudar a opção de limpeza de tela de acordo com o sistema que estiver usando.
		//system("cls");
		system("clear");
	}
}
 

 //funcao mover/imprimir meteoro no mapa
 void mov_meteoro(char mapSpace[LIN][COL], meteoro_t p[METQ], int *contL, int *contC, int *quantMeteoro){
 		
 	char hit = 0;

 	int aux;

			for(aux = 0; aux < (*quantMeteoro); aux++){

				if(*contL == p[aux].yML && *contC == p[aux].xMC && p[aux].xMC != 0 && p[aux].xMC != 1 ){

					mapSpace[p[aux].yML][p[aux].xMC] = ' ';
					//if(mapSpace[p[aux].yML][p[aux].xMC+1] != ' ') hit = 1;
					mapSpace[p[aux].yML][p[aux].xMC+1] = '(';
					//if(mapSpace[p[aux].yML][p[aux].xMC+2] != ' ') hit = 1;
					mapSpace[p[aux].yML][p[aux].xMC+2] = '@';
					//if(mapSpace[p[aux].yML][p[aux].xMC+3] != ' ') hit = 1;
					mapSpace[p[aux].yML][p[aux].xMC+3] = '@';
					//if(mapSpace[p[aux].yML][p[aux].xMC+4] != ' ') hit = 1;
					mapSpace[p[aux].yML][p[aux].xMC+4] = '@';
					//if(mapSpace[p[aux].yML][p[aux].xMC+5] != ' ') hit = 1;
					mapSpace[p[aux].yML][p[aux].xMC+5] = ')';
					//if(mapSpace[p[aux].yML][p[aux].xMC+6] != ' ') hit = 1;
					mapSpace[p[aux].yML][p[aux].xMC+6] = '#';
					//if(mapSpace[p[aux].yML][p[aux].xMC+7] != ' ') hit = 1;
					mapSpace[p[aux].yML][p[aux].xMC+7] = '#';
					//if(mapSpace[p[aux].yML][p[aux].xMC+8] != ' ') hit = 1;
					mapSpace[p[aux].yML][p[aux].xMC+8] = '#';

					//if(mapSpace[p[aux].yML+1][p[aux].xMC+1] != ' ') hit = 1;
					mapSpace[p[aux].yML+1][p[aux].xMC] = '(';
					mapSpace[p[aux].yML+1][p[aux].xMC+1] = '@';
					mapSpace[p[aux].yML+1][p[aux].xMC+2] = '@';
					mapSpace[p[aux].yML+1][p[aux].xMC+3] = '@';
					mapSpace[p[aux].yML+1][p[aux].xMC+4] = ')';
					mapSpace[p[aux].yML+1][p[aux].xMC+5] = '#';
					mapSpace[p[aux].yML+1][p[aux].xMC+6] = '#';
					mapSpace[p[aux].yML+1][p[aux].xMC+7] = '#';
					mapSpace[p[aux].yML+1][p[aux].xMC+8] = ' ';

					mapSpace[p[aux].yML+2][p[aux].xMC] = ' ';
					//if(mapSpace[p[aux].yML+2][p[aux].xMC+1] != ' ') hit = 1;
					mapSpace[p[aux].yML+2][p[aux].xMC+1] = '(';
					//if(mapSpace[p[aux].yML+2][p[aux].xMC+2] != ' ') hit = 1;
					mapSpace[p[aux].yML+2][p[aux].xMC+2] = '@';
					//if(mapSpace[p[aux].yML+2][p[aux].xMC+3] != ' ') hit = 1;
					mapSpace[p[aux].yML+2][p[aux].xMC+3] = '@';
					//if(mapSpace[p[aux].yML+2][p[aux].xMC+4] != ' ') hit = 1;
					mapSpace[p[aux].yML+2][p[aux].xMC+4] = '@';
					//if(mapSpace[p[aux].yML+2][p[aux].xMC+5] != ' ') hit = 1;
					mapSpace[p[aux].yML+2][p[aux].xMC+5] = ')';
					//if(mapSpace[p[aux].yML+2][p[aux].xMC+6] != ' ') hit = 1;
					mapSpace[p[aux].yML+2][p[aux].xMC+6] = '#';
					//if(mapSpace[p[aux].yML+2][p[aux].xMC+7] != ' ') hit = 1;
					mapSpace[p[aux].yML+2][p[aux].xMC+7] = '#';
					//if(mapSpace[p[aux].yML+2][p[aux].xMC+8] != ' ') hit = 1;
					mapSpace[p[aux].yML+2][p[aux].xMC+8] = '#';


				}

						
			}

			

}






/*			

	ESPAÇONAVE CHAVOSO!

            \ \_____
         ###[==_____>
            / / 


	METEORO DE PEGASUS!

		 (@@@)###
		(@@@)###
		 (@@@)###

*/
